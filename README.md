# Sync master with develop and create releases

> :warning: **the sync command will delete the following branches locally and remotely:** `sync-develop-master`
> :warning: **the release command will delete the following branches locally and remotely based on the third arg:** `gitflow release release/v1.0.0; # -> release/v1.0.0 gets deleted`

Not much info here.

### [LICENSE](./LICENSE)
