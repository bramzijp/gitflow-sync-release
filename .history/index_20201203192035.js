// git fetch origin;
// git branch -D sync-develop-master;
// git push origin --delete sync-develop-master --no-verify;
// git checkout -b sync-develop-master;
// git merge --no-ff origin/master --no-verify;
// git push --set-upstream origin sync-develop-master --no-verify;

const defaultSrcBranchName = "sync-develop-master";

const execHandler = (err, stdout, stderr) => {
  if (err) {
    throw err;
  }
};

const sync = ({ srcBranchName }) => {
  const syncExecList = [
    "git fetch origin",
    "git branch -D sync-develop-master",
    "git push origin --delete sync-develop-master --no-verify",
    "git checkout -b sync-develop-master origin/develop",
    "git merge --no-ff origin/master --no-verify",
    "git push --set-upstream origin sync-develop-master --no-verify",
  ];

  exec();
};

const release = ({ branchName, srcBranchName }) => {
  const releaseExecList = [
    "git fetch origin",
    `git branch -D ${branchName}`,
    `git push origin --delete ${branchName} --no-verify`,
    `git checkout -b ${branchName} origin/develop`,
  ];
};
