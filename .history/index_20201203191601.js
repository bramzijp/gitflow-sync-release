// git fetch origin;
// git branch -D sync-develop-master;
// git push origin --delete sync-develop-master --no-verify;
// git checkout -b sync-develop-master;
// git merge --no-ff origin/master --no-verify;
// git push --set-upstream origin sync-develop-master --no-verify;

const execHandler = (err, stdout, stderr) => {
  if (err) {
    throw err;
  }
};

const sync = () => {
  const syncExecList = [
    "git fetch origin",
    "git branch -D sync-develop-master",
    "git push origin --delete sync-develop-master --no-verify",
    "git checkout -b sync-develop-master",
    "git merge --no-ff origin/master --no-verify",
    "git push --set-upstream origin sync-develop-master --no-verify",
  ];

  exec();
};
