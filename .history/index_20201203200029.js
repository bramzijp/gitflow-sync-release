const fs = require("fs");
const path = require("path");
const defaultSrcBranchName = "sync-develop-master";
const defaultMasterBranchName = "master";
const defaultDevelopBranchName = "develop";
const configLocation = path.join(process.cwd(), "./gitflow.json");

const args = process.argv.slice(2);

const getConfig = () => {
  try {
    // catch
    const test = "";
  } catch (_) {
    // lol
  }
};

const runExecs = (execStrings) => {
  for (let i = 0; i < execStrings.length; i++) {
    execSync(execStrings[i]);
  }
};

const sync = ({ srcBranchName, developBranchName }) => {
  const syncExecList = [
    "git fetch origin",
    `git branch -D ${srcBranchName}`,
    `git push origin --delete ${srcBranchName} --no-verify`,
    `git checkout -b ${srcBranchName} origin/${developBranchName}`,
    "git merge --no-ff origin/master --no-verify",
    `git push --set-upstream origin ${srcBranchName} --no-verify`,
  ];

  runExecs(syncexecSync);
};

const release = ({ branchName, srcBranchName, developBranchName }) => {
  const releaseExecList = [
    "git fetch origin",
    `git branch -D ${branchName}`,
    `git push origin --delete ${branchName} --no-verify`,
    `git checkout -b ${branchName} origin/${developBranchName}`,
  ];

  runExecs(syncExecList);
};

const main = () => {
  const config = getConfig();

  const argData = {};
  const isSync = args.includes("sync");
  const isRelease = args.includes("release");
  const isSyncRelease = isSync && isRelease;

  const lastArg = args[args.length - 1];

  if (isRelease) {
  }
};
