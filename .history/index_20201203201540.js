const fs = require("fs");
const path = require("path");
const readline = require("readline");

const rl = readline.createInterface(process.stdin, process.stdout);

const args = process.argv.slice(2);

const configLocation = path.join(process.cwd(), "./gitflow.json");

const defaultConfig = {
  srcBranchName: "sync-develop-master",
  masterBranchName: "master",
  developBranchName: "develop",
  protectedBranches: ["master", "develop", "main", "sync-develop-master"],
  exitWhenProtected: true,
};

const saveExit = () => {
  console.warn("Nothing has been changed. Exiting.");
};

const getConfig = () => {
  try {
    const userConfig = JSON.parse(fs.readSync(configLocation));
    const updatedConfig = {
      ...defaultConfig,
      ...userConfig,
      protectedBranches:
        userConfig.protectedBranches || defaultConfig.protectedBranches,
    };
    return updatedConfig;
  } catch (_) {
    return defaultConfig;
  }
};

const runExecs = (execStrings) => {
  for (let i = 0; i < execStrings.length; i++) {
    execSync(execStrings[i]);
  }
};

const sync = ({ srcBranchName, developBranchName }) => {
  const syncExecList = [
    "git fetch origin",
    `git branch -D ${srcBranchName}`,
    `git push origin --delete ${srcBranchName} --no-verify`,
    `git checkout -b ${srcBranchName} origin/${developBranchName}`,
    "git merge --no-ff origin/master --no-verify",
    `git push --set-upstream origin ${srcBranchName} --no-verify`,
  ];

  runExecs(syncexecSync);
};

const release = ({ branchName, srcBranchName, developBranchName }) => {
  const releaseExecList = [
    "git fetch origin",
    `git branch -D ${branchName}`,
    `git push origin --delete ${branchName} --no-verify`,
    `git checkout -b ${branchName} origin/${developBranchName}`,
  ];

  runExecs(syncExecList);
};

const main = () => {
  const config = getConfig();
  const { protectedBranches } = config;
  const argData = {};
  const isSync = args.includes("sync");
  const isRelease = args.includes("release");
  const isSyncRelease = isSync && isRelease;

  const lastArg = args[args.length - 1];
  r;

  if (isRelease && lastArg.includes(protectedBranches)) {
    if (exitWhenProtected) {
      saveExit();
    }
    rl.question(
      "DANGER you might overwite a protected branch? [no]/[yes]: ",
      function (answer) {
        if (answer === "yes" || answer === "y") {
          console.log("Nothing has been changed. Exiting." + filename);
        } else {
          console.log("Overwriting " + filename);
          fs.writeFile(filename, data, done);
        }
      }
    );
  }
};
