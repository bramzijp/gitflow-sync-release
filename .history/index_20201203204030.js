const fs = require("fs");
const path = require("path");
const readline = require("readline");

const rl = readline.createInterface(process.stdin, process.stdout);

const args = process.argv.slice(2);
const lastArg = args[args.length - 1];

const configLocation = path.join(process.cwd(), "./gitflow.json");

const defaultConfig = {
  srcBranchName: "sync-develop-master",
  masterBranchName: "master",
  developBranchName: "develop",
  protectedBranches: ["master", "develop", "main", "sync-develop-master"],
  exitWhenProtected: true,
  gitlabMrBase: "https://",
};


const protectedBranchBaseError = `You attempted to overwrite the protected branch: "${lastArg}".`;

const saveExit = () => {
  console.warning("Nothing has been changed. Exiting.");
  process.exit(1);
};

const getConfig = () => {
  try {
    const userConfig = JSON.parse(fs.readSync(configLocation));
    const updatedConfig = {
      ...defaultConfig,
      ...userConfig,
      protectedBranches:
        userConfig.protectedBranches || defaultConfig.protectedBranches,
    };
    return updatedConfig;
  } catch (_) {
    return defaultConfig;
  }
};

const config = getConfig();

const runExecs = (execStrings) => {
  let execs = [];
  for (let i = 0; i < execStrings.length; i++) {
    const syncExec = execSync(execStrings[i]);
    execs.push(syncExec);
  }
  return execs;
};

const protectRelease = ({ protectedBranches }) => {
  const isInvalid = lastArg.includes(protectedBranches);

  if (isInvalid && exitWhenProtected) {
    console.info(protectedBranchBaseError);
    saveExit();
  }

  if (isInvalid) {
    rl.question(
      `DANGER ${protectedBranchBaseError} Do you want to continue? [no]/yes: `,
      (answer) => {
        // not yes -> exit
        if (answer !== "yes" || answer !== "y") {
          saveExit();
        }
      }
    );
  }
};

const sync = ({ srcBranchName, developBranchName, gitlabMrBase }) => {
  const syncExecList = [
    "git fetch origin",
    `git branch -D ${srcBranchName}`,
    `git push origin --delete ${srcBranchName} --no-verify`,
    `git checkout -b ${srcBranchName} origin/${developBranchName}`,
    "git merge --no-ff origin/master --no-verify",
    `git push --set-upstream origin ${srcBranchName} --no-verify`,
  ];

  const execs = runExecs(syncexecSync);

  if ()
};

const release = ({ branchName, srcBranchName, developBranchName }) => {
  const releaseExecList = [
    "git fetch origin",
    `git branch -D ${branchName}`,
    `git push origin --delete ${branchName} --no-verify`,
    `git checkout -b ${branchName} origin/${developBranchName}`,
  ];

  runExecs(syncExecList);
};

const main = () => {
  const isSync = args.includes("sync");
  const isRelease = args.includes("release");

  if (isRelease) protectRelease(config);

  if (isSync) sync(config);
  if (isRelease) release(config);
};

main();
