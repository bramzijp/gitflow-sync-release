const defaultSrcBranchName = "sync-develop-master";
const defaultMasterBranchName = "master";
const defaultDevelopBranchName = "develop";
const args = process.argv.slice(2);
console.log(args);
const runExecs = (execStrings) => {
  for (let i = 0; i < execStrings.length; i++) {
    execSync(execStrings[i]);
  }
};

const sync = ({
  srcBranchName = defaultSrcBranchName,
  developBranchName = defaultDevelopBranchName,
}) => {
  const syncExecList = [
    "git fetch origin",
    `git branch -D ${srcBranchName}`,
    `git push origin --delete ${srcBranchName} --no-verify`,
    `git checkout -b ${srcBranchName} origin/${developBranchName}`,
    "git merge --no-ff origin/master --no-verify",
    `git push --set-upstream origin ${srcBranchName} --no-verify`,
  ];

  runExecs(syncexecSync);
};

const release = ({
  branchName = defaultSrcBranchName,
  srcBranchName = defaultDevelopBranchName,
  developBranchName = defaultDevelopBranchName,
}) => {
  const releaseExecList = [
    "git fetch origin",
    `git branch -D ${branchName}`,
    `git push origin --delete ${branchName} --no-verify`,
    `git checkout -b ${branchName} origin/${developBranchName}`,
  ];

  runExecs(syncExecList);
};

const contains;

const main = () => {
  const isSync = args.includes("sync");
  const isRelease = args.includes("release");
  const isSyncRelease = isSync && isRelease;
  if (isRelease && !isSyncRelease) {
  }
};
